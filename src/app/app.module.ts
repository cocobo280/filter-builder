import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';  
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { RouterModule } from '@angular/router';

/* services */
import { ApiService } from "./shared/services/api.service";
import { AuthService } from "./shared/services/auth.service";
import { ConfigService } from "./shared/services/config.service";
import { FormService } from "./shared/services/form.service";
import { GenericModuleService } from "./shared/services/genericModule.service";
import { FilterBuilderComponent } from './filter-builder/filter-builder/filter-builder.component';
import { FormsModule  } from "@angular/forms";
import { QueryBuilderModule } from "angular2-query-builder";
import { ImporterModule } from "./importer/importer.module";

const APP_PROVIDERS = [
  ApiService,
  AuthService,
  ConfigService,
  GenericModuleService,
  FormService,
];

@NgModule({
  declarations: [
    AppComponent,
    FilterBuilderComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule,
    FormsModule,
    QueryBuilderModule,
    ImporterModule,
    CommonModule
  ],
  bootstrap: [AppComponent],
  providers: [
    APP_PROVIDERS,
    HttpClientModule
  ]
})
export class AppModule { }
