import { Component, OnInit } from '@angular/core';
import { QueryBuilderConfig } from 'angular2-query-builder';
import { ConfigService } from '../../shared/services/config.service';

type Rule = {
  field: string
  operator: string
  value: string
}

type Group = {
  condition: "and" | "or"
  rules: Array<Rule | Group>
}

type Condition = {
  [x: string]: {
    [operator: string]: string
  }
}

type And = {
  and?: Filter[]
}
type Or = {
  or?: Filter[]
}

type Filter = Condition | And | Or

@Component({
  selector: 'app-filter-builder',
  templateUrl: './filter-builder.component.html',
  styleUrls: ['./filter-builder.component.scss']
})

export class FilterBuilderComponent implements OnInit {

  query: any = {
    condition: "and",
    rules: []
  };
  _operators = {
    string: ['=', '!=', 'contains', 'like'],
    number: ['=', '!=', '>', '>=', '<', '<='],
    time: ['=', '!=', '>', '>=', '<', '<='],
    date: ['=', '!=', '>', '>=', '<', '<='],
    category: ['=', '!=', 'in', 'not in'],
    multiSelect: ['=', '!=', 'in', 'not in'],
    boolean: ['=']
  }
  _types = {
    text: "string",
    number: "number",
    date: "date",
    select: "category",
    radio: "category",
    multiSelect: "multiselect"
  }
  readonly operatorsLB = {
    "like": "contains",
    "eq": "=",
    "neq": "!=",
    "nlike": "notcontains",
    "gt": ">",
    "gte": ">=",
    "lt": "<",
    "between": "between",
    "lte": "<="
  }

  readonly operatorsFB = {
    "contains": "like",
    "=": "eq",
    "!=": "neq",
    ">": "gt",
    ">=": "gte",
    "<": "lt",
    "between": "between",
    "<=": "lte"
  }
  config: QueryBuilderConfig = {
    fields: {}
  }
  done: boolean = false;

  constructor(private configService: ConfigService) { }

  ngOnInit(): void {
    this.configService.configEventEmitter.subscribe(el => {
      this.config.fields = { ...this.parsePeopleConfigToFields(), ...this.parsePeopleRelationsToFields() };
      this.done = true
    })
    this.configService.getUserConfig()
  }

  parsePeopleConfigToFields() {
    let fields = this.configService.getFieldsOfForm('peoples').filter(el => el.filterable == true)
    return fields.reduce((acc, el) => ({
      ...acc, [el.name]: {
        name: el.name,
        operators: this._operators[this._types[el.type]],
        type: this._types[el.type],
        ...el.type == "select" || el.type == "multiSelect" || el.type == "radio" ? { options: el.options } : null
      }
    }), {});
  }

  parsePeopleRelationsToFields() {
    let relations = this.configService.getModuleConfig('peoples').relations
    let relationsFields = {};
    Object.keys(relations).forEach(el => {
      let fields = this.configService.getFieldsOfForm(relations[el].module).filter(el => el.filterable == true)
      let obj = fields.reduce((acc, el2) => ({
        ...acc, [el + "." + el2.name]: {
          name: el + "." + el2.name,
          operators: this._operators[this._types[el2.type]],
          type: this._types[el2.type],
          ...el2.type == "select" || el2.type == "multiSelect" || el2.type == "radio" ? { options: el2.options } : null
        }
      }), {})
      relationsFields = { ...relationsFields, ...obj }
    })
    return relationsFields
  }


  transformQuery(group: Group): Filter {
    const wholeQuery: Filter = {}

    for (const rule of group.rules) {
      let query: Filter = {}

      if ('condition' in rule) {
        query[rule.condition] = Object.entries(this.transformQuery(rule)).map(([key, value]) => ({ [key]: value }))
      } else {
        query[rule.field] = {
          [this.operatorsFB[rule.operator]]: rule.value ? rule.value.replace(/([.*+\-?^${}()|[\]\\])/g, '\\$1') : rule.value
        }
      }

      Object.assign(wholeQuery, query)
    }
    return wholeQuery
  }

  transformWhere(group: Filter): Group {
    if ("and" in group) {
      const filter: And = group
      return { condition: "and", rules: filter.and.map(el => this.transformWhere(el)) }
    }
    if ("or" in group) {
      const filter: Or = group
      return { condition: "or", rules: filter.or.map(el => this.transformWhere(el)) }
    }
    const condition = group as Condition;

    const rules: Rule[] = Object.entries(condition).map(([field, obj]) => {
      const operator = this.operatorsLB[Object.keys(obj)[0]]

      return {
        field,
        operator,
        value: obj[Object.keys(obj)[0]] ? obj[Object.keys(obj)[0]].replace(/[\\]/g, '') : obj[Object.keys(obj)[0]]
      }
    })
    return { condition: "and", rules }
  }





  pave() {
    console.log(this.query);    
    console.log(this.transformQuery(this.query));
    console.log(this.transformWhere(this.transformQuery(this.query)));

  }

}
