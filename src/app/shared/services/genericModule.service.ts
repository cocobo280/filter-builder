import { Injectable, EventEmitter } from '@angular/core';
import { ApiService } from './api.service';
import { ConfigService } from './config.service'

@Injectable()
export class GenericModuleService {

    public cloneData: any = {};
    public data: any = {};
    public saveTemplate: EventEmitter<boolean> = new EventEmitter();
    public loadTemplate: EventEmitter<any> = new EventEmitter();

    constructor(
        private apiService: ApiService,
        private configService: ConfigService,
    ) { }

    post(body: object, controller?: string) {
        let _module = controller ? controller : this.configService.getCurrentModule();
        return this.apiService.post(body, _module)
    }

    get(controller?: string, id?: string, params?: string) {
        let _module = controller ? controller : this.configService.getCurrentModule();
        return this.apiService.get(_module, id, params)
    }

    getPivot(controller: string) {
        return this.apiService.getPivot(controller)
    }

    delete(controller?: string, id?: string) {
        let _module = controller ? controller : this.configService.getCurrentModule();
        return this.apiService.delete(_module, id)
    }

    update(body: any, id: string, controller?: string) {
        let _module = controller ? controller : this.configService.getCurrentModule();
        return this.apiService.put(body, _module, id)
    }

    patch(body: any, id: string, controller?: string) {
        let _module = controller ? controller : this.configService.getCurrentModule();
        return this.apiService.patch(body, _module, id)
    }

    put(body: any, id: string, controller?: string) {
        let _module = controller ? controller : this.configService.getCurrentModule();
        return this.apiService.put(body, _module, id)
    }

    setCloneData(data) {
        if (data) {
            delete data.id;
            delete data.history;
            delete data.offer;
            delete data.reservation;
            delete data.promise;
            delete data.protocolization;
            delete data.transactionDate;
            delete data.stage;
            delete data.amountPaid;
            delete data.amountPending;
            delete data.createdAt;
            delete data.updatedAt;
            this.cloneData = data;      
        } else {
            this.cloneData = null;
        }
    }

    getCloneData() {
        return this.cloneData ? this.cloneData : null
    }

    isPeople() {
        let mdl = this.configService.getCurrentModule()
        if (mdl == 'contacts' || mdl == 'leads') {
            return true
        }
    }

    isClient() {
        let mdl = this.configService.getCurrentModule()
        if (mdl == 'customers') {
            return true
        }
    }

    isTransactions() {
        let mdl = this.configService.getCurrentModule()
        if (mdl == 'transactions') {
            return true
        }
    }

    isProduct() {
        let mdl = this.configService.getCurrentModule()
        if (mdl == 'products') {
            return true
        }
    }

    setData(key: string, data: any, start?: number, list?: boolean) {
        if (list) {
            //          let savedData: any[] = this.getData(key);
            this.data[key] ? null : this.data[key] = [];
            this.data[key].splice(start, data.length, ...data);
            //          savedData.splice(start, data.length, ...data);
            //          !start || start == 0 ? this.saveOnLocalStorage(key, savedData) : null
        } else {
            this.data[key] = data;
            //            this.saveOnLocalStorage(key, data)
        }
    }

    deleteFromData(key: string, id) {
        this.data[key] = this.data[key].filter(el => el.id != id);
    }

    setFilteredData(key: string, data: any) {
        this.data["filter" + key] = data;
        //this.saveOnLocalStorage("filter" + key, data)
    }

    setItemById(id, data, key?: string) {
        if (this.data[key]) {
            let item = this.data[key].find((el, index) => {
                if (el.id == id) {
                    this.data[key][index] = data
                }
                return el.id == id
            })
            item ? null : this.data[key].push(data)

        } else {
            let array = []
            array.push(data)
            this.data[key] = array;
        }
    }

    getItemById(id, key?: string) {
        let item
        if (this.data[key]) {
            item = this.data[key].find(el => {
                return el.id == id
            })
        }
        return item
    }

    getData(key: string) {
        return this.data[key] ? this.data[key] : []
        //return this.data[key] ? this.data[key] : this.getFromLocalStorage(key) ? this.getFromLocalStorage(key) : []
    }

    getFilteredData(key: string) {
        return this.data["filter" + key] ? this.data["filter" + key] : []
        //return this.data["filter" + key] ? this.data["filter" + key] : this.getFromLocalStorage("filter" + key) ? this.getFromLocalStorage("filter" + key) : []
    }

    saveOnLocalStorage(key: string, data: any) {
        localStorage.setItem(key, JSON.stringify(data))
    }

    getFromLocalStorage(key: string) {
        return JSON.parse(localStorage.getItem(key)) || null
    }

    emitSaveTemplate() {
        this.saveTemplate.emit(true);
    }

    emitTemplate(template: any) {
        this.loadTemplate.emit(template);
    }
}