import { Injectable, EventEmitter } from '@angular/core';
import { ConfigService } from "./config.service";

@Injectable({
  providedIn: 'root'
})
export class FormService {
  public newValueEmitter: EventEmitter<any> = new EventEmitter();
  constructor(private configService: ConfigService) { }

  correctValue(value, field) {
    this.newValueEmitter.emit({ value: value, field: field })
  }

  parseTypeOfValue(data: any, module?: string): any {
    let obj: any = {};
    let fields = this.getFields(module).reduce((acc, el) => ({ ...acc, [el.name]: el.type }), {})
    Object.keys(data).forEach(el => {
      if (data[el]) {
        switch (fields[el]) {
          case "date":
            obj[el] = new Date(data[el])
            break;

          case "text":
            obj[el] = data[el].toString()
            break;

          case "number":
            obj[el] = parseFloat(data[el])
            break;

          default:
            break;
        }
      }
    })
    return obj
  }

  getFields(module?: string): any[] {
    return this.configService.getFormConfig(module).sections.reduce((acc, el) => (acc.concat(el.fields)), [])
  }



}
