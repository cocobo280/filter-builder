import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ConfigService {
    public config: any;
    public modl: string = '';
    public configEventEmitter: EventEmitter<boolean> = new EventEmitter;
    public routerEventEmitter: EventEmitter<boolean> = new EventEmitter
    public url: string = '';
    public counter: number = 0;

    constructor(
        private http: HttpClient,
        //private apiService: ApiService,
    ) {

    }

    getUserConfig() {
        this.http.get('assets/config/config.json').subscribe(config => {  
            this.setConfig(config)
        })
    }

    getModules() {
        let modules = []
        this.config.sections.forEach(element => {
            element.modules.map(el => {
                modules.push(el)
            })
        });
        return modules.filter(el => { if (el.name != 'campaings' && el.name != 'files') { return el } })
    }

    setModule(modl, url?) {
        this.modl = modl ? modl : this.modl;
        url ? this.setUrl(url) : null;
    }

    setUrl(url) {
        this.url = url ? url : this.url;
    }

    getUrl() {
        return this.url
    }

    checkConfig() {
        this.config ? this.configEventEmitter.emit(true) : this.configEventEmitter.emit(false)
    }

    checkConfigForRotes() {
        this.config ? this.routerEventEmitter.emit(true) : this.routerEventEmitter.emit(false)
    }

    setConfig(config) {
        this.config = config;
        this.configEventEmitter.emit(true)
        
    }

    getModuleConfig(modl?) {
        // modl == 'people' ? modl = 'contacts' : null;
        let modules = this.getModules();
        let name = modl ? modl : this.modl;
        let modlConfig = modules.find(el => { return el.name == name })
        return modlConfig
    }

    getEditConfig(modl?) {
        let config: any = this.getModuleConfig(modl)
        let editConfig = []
        editConfig = config.edit ? config.edit.actions : []
        return editConfig
    }

    getCurrentModule() {
        return this.modl
    }


    getConfig() {
        return this.config
    }

    isGeneric(): boolean {
        if (this.modl != '' && this.url != '') {
            return true
        } else {
            return false
        }
    }

    getFormConfig(module?) {
        return this.getModuleConfig(module).form
    }

    getFieldsOfForm(module?) {
        return this.getFormConfig(module).sections.reduce((acc, el) => (acc.concat(el.fields)), [])
    }

    getFieldConfig(name: string, module?) {
        return this.getFieldsOfForm(module).find(el => el.name == name)
    }

    getAllFieldConfig(name: string, module?) {
        return this.getFieldsOfForm(module).filter(el => el.name == name)
    }
}


