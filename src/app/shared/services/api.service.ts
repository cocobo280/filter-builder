import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { apiEndPoint } from "../utilities/api-url";

declare let swal: any;

@Injectable()
export class ApiService {
  private apiEndPoint: string = apiEndPoint.url;
  private apiEndPointPivot: string = '';

  constructor(
    private http: HttpClient,
    private authService: AuthService
  ) { }

  post(body: object, controller: string, headers?: HttpHeaders) {
    return this.http.post(this.apiEndPoint + controller, body, { headers: headers ? headers : this.getHeader() })
  }

  get(controller: string, id?: string, params?: string) {
    id = id ? '/' + id : '';
    params = params ? params : '';
    return this.http.get(this.apiEndPoint + controller + id + params, { headers: this.getHeader() })
  }

  getPivot(controller: string) {
    return this.http.get(this.apiEndPointPivot + controller)
  }

  put(body: object, controller: string, id: string) {
    return this.http.put(this.apiEndPoint + controller + '/' + id, body, { headers: this.getHeader() })
  }

  delete(controller: string, id: string) {
    id = id ? '/' + id : '';
    return this.http.delete(this.apiEndPoint + controller + id, { headers: this.getHeader() })
  }

  patch(body: object, controller: string, id: string) {
    return this.http.patch(this.apiEndPoint + controller + '/' + id, body, { headers: this.getHeader() })
  }

  getHeader() {
    let header: HttpHeaders = new HttpHeaders();
    header = header.append('Content-Type', 'application/json');
    header = header.append('Access-Control-Allow-Origin', '*');
    header = header.append('Authorization', 'Bearer ' + this.authService.getToken());
    return header
  }


}
