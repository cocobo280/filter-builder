import { Injectable, EventEmitter } from '@angular/core';
import { Router } from "@angular/router";
import { HttpClient } from '@angular/common/http';
import { apiEndPoint } from "../../shared/utilities/api-url";

declare let Messenger: any;

@Injectable()
export class AuthService {
    public token: string = '';
    public user: any;
    private apiEndPoint: string = apiEndPoint.url;
    public userInfoChange: EventEmitter<any> = new EventEmitter();


    constructor(
        private router: Router,
        private http: HttpClient,

    ) { }

    login(body) {
        return this.http.post(this.apiEndPoint + 'auth/signin', body)
    }

    setAuthData(data: any) {
        this.setToken(data.token);
        this.setUser(data.user)
    }

    setToken(token: string) {
        this.token = token;
        localStorage.setItem('token', token)
    }

    setUser(user: any) {
        this.user = user;
        localStorage.setItem('user', JSON.stringify(user))
        this.userInfoChange.emit(this.getUser())
    }

    getToken(): string {
        this.token = this.token ? this.token : localStorage.getItem('token')
        return this.token;
    }

    getUser() {
        this.user = this.user ? this.user : JSON.parse(localStorage.getItem('user'))
        return this.user
    }

    checkToken(): boolean {
        let checked = localStorage.getItem('token') ? true : false
        return checked
    }

    logout() {
        this.router.navigate(['/login']);
        localStorage.clear();
    }

    isAdmin(): boolean {
        return this.getUser().level == 'administrator'
    }

    isAgent(): boolean {
        return this.getUser().level == 'agent'
    }

    isFranchise(): boolean {
        return this.getUser().level == 'franchise'
    }

    checkEmailCredentials(withOutMessage?:boolean):boolean {
        // if (!this.user.credentials) {
        //     if (!withOutMessage) {
        //         let msg = Messenger({ extraClasses: 'messenger-fixed messenger-on-top' }).post({
        //             message: this.translateService.instant('imbox.noCredentials'),
        //             id: "Only-one-message",
        //             type: 'error',
        //             hideAfter: 100,
        //             actions: {
        //                 retry: {
        //                     label: this.translateService.instant('imbox.setNow'),
        //                     action: function () {
        //                         this.router.navigate(['/data/sources/email'])
        //                         msg.hide()
        //                     }.bind(this)
        //                 },
        //                 cancel: {
        //                     label: this.translateService.instant('imbox.setLater'),
        //                     action: function () {
        //                         msg.hide()
        //                     }
        //                 }
        //             }
        //         })
        //     }            
        //     return false
        // }else{
        //     return true
        // }
        return true
    }
}
