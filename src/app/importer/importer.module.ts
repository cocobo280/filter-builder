import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImporterComponent } from './importer/importer.component';


@NgModule({
  declarations: [ImporterComponent],
  imports: [
    CommonModule
  ],
  exports: [ImporterComponent, CommonModule]
})
export class ImporterModule { }
