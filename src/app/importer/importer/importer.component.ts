import { Component, OnInit } from '@angular/core';
import { error } from 'jquery';
import * as XLSX from 'xlsx'

@Component({
  selector: 'app-importer',
  templateUrl: './importer.component.html',
  styleUrls: ['./importer.component.scss']
})
export class ImporterComponent implements OnInit {
  data: any[] ;

  constructor() { }

  ngOnInit(): void {
  }

  onFileChange(event) {
    console.log("ele evento es", event);
    const target: DataTransfer = <DataTransfer>(event.target);
    if (target.files.length > 1) throw new Error; ('Cannot use multiple files')
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      const binaryString: string = e.target.result;
      const workBook: XLSX.WorkBook = XLSX.read(binaryString, { type: 'binary' })
      const workBookName: string = workBook.SheetNames[0];
      const workSheet: XLSX.WorkSheet = workBook.Sheets[workBookName];
      this.data = (XLSX.utils.sheet_to_json(workSheet, { header: 1 }))
      console.log(workSheet);
      console.log("data", this.data);
    }

    reader.readAsBinaryString(target.files[0]);
  }
}
